const express = require('express');

const app = express();

app.use(express.json());
app.use(require('./app/routes.js'));

app.listen(3333);
