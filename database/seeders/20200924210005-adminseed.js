module.exports = {
  up: async (queryInterface) => queryInterface.bulkInsert('Users', [{
    id: 1000,
    name: 'Admin',
    email: 'admin@admin.com',
    password: '$2a$08$HmmvFrevXAP2kzw/MfBtf.BHUHb5tEwGQE3M97A9ecYxPGzNKF6wS', // 123
    role: 'Admin',
    createdAt: new Date(),
    updatedAt: new Date(),
    isDeleted: false,

  }], {}),

  down: async (queryInterface) => {
    await queryInterface.bulkDelete('Users', { id: 1000 }, {});
  },
};
