module.exports = {
  up: async (queryInterface, DataTypes) => queryInterface.createTable('Movies', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER,
    },
    name: {
      allowNull: false,
      type: DataTypes.STRING,
    },
    director: {
      allowNull: true,
      type: DataTypes.STRING,
    },
    writer: {
      allowNull: true,
      type: DataTypes.STRING,
    },
    category: {
      allowNull: true,
      type: DataTypes.STRING,
    },
    isDeleted: {
      allowNull: false,
      type: DataTypes.BOOLEAN,
      defaultValue: false,
    },
    createdAt: {
      allowNull: false,
      type: DataTypes.DATE,
    },
    updatedAt: {
      allowNull: false,
      type: DataTypes.DATE,
    },
  }),

  down: async (queryInterface) => queryInterface.dropTable('Movies'),
};
