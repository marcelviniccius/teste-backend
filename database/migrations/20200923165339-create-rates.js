module.exports = {
  up: async (queryInterface, DataTypes) => queryInterface.createTable('Rates', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER,
    },
    idMovie: {
      allowNull: false,
      type: DataTypes.INTEGER,
      references: { model: 'Movies', key: 'id' },
    },
    idUser: {
      allowNull: true,
      type: DataTypes.INTEGER,
      references: { model: 'Users', key: 'id' },
    },
    isDeleted: {
      allowNull: true,
      type: DataTypes.BOOLEAN,
      defaultValue: false,
    },
    rate: {
      allowNull: true,
      type: DataTypes.INTEGER,
    },

    createdAt: {
      allowNull: false,
      type: DataTypes.DATE,
    },
    updatedAt: {
      allowNull: false,
      type: DataTypes.DATE,
    },
  }),

  down: async (queryInterface) => queryInterface.dropTable('Rates'),
};
