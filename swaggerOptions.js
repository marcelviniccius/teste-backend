module.exports = {
  swaggerDefinition: {
    info: {
      version: '1.0.0',
      title: 'Desafio API',
      description: 'Desafio API Desenvolvida para Ioasys',
      contact: {
        name: 'Marcel',
      },
      servers: ['http://localhost:3333'],

    },
  },
  // ['.routes/*.js']
  apis: ['./app/routes.js'],
};
