# Instruções Marcel
-Configure um .env com as variaveis listadas no .env.example
-Rode as migrations com o comando: npm run db:migrate
-Rode as seeds com o comando:npm run db:seed:all
-Rode a api com o comando:npm run start:dev 
-Para ver a documentação da api acesse /api-docs
-Para logar com o adm criado na seed use o email:admin@admin.com e senha:123

Até mais!
  
# Sobre

Estes documento README tem como objetivo fornecer as informações necessárias para realização do projeto de avaliação de candidatos.

# 🏗 O que fazer?

- Você deve realizar um fork deste repositório e, ao finalizar, enviar o link do seu repositório para a nossa equipe. Lembre-se, NÃO é necessário criar um Pull Request para isso, nós iremos avaliar e retornar por email o resultado do seu teste.

# 🚨 Requisitos

- A API deverá ser construída em **NodeJS** ou **Rails** Ok!(NodeJS)
- Implementar autenticação e deverá seguir o padrão **JWT**, lembrando que o token a ser recebido deverá ser no formato **Bearer** Ok!
- Caso seja desenvolvida em NodeJS o seu projeto terá que ser implementado em **ExpressJS** ou **SailsJS** Ok(ExpressJS)!
- Para a comunicação com o banco de dados utilize algum **ORM**/**ODM** Ok!(Sequelize)
- Bancos relacionais permitidos:
  - MySQL
  - MariaDB
  - Postgre Ok!
- Bancos não relacionais permitidos:
  - MongoDB
- Sua API deverá seguir os padrões Rest na construção das rotas e retornos Ok!
- Sua API deverá conter a collection/variáveis do postman ou algum endpoint da documentação em openapi para a realização do teste Ok!

# 🕵🏻‍♂️ Itens a serem avaliados

- Estrutura do Projeto
- Segurança da API, como autenticação, senhas salvas no banco, SQL Injection e outros
- Boas práticas da Linguagem/Framework
- Seu projeto deverá seguir tudo o que foi exigido na seção [O que desenvolver?](##--o-que-desenvolver)
- Migrations para a criação das tabelas do banco relacional Ok!

# 🎁 Extra

Esses itens não são obrigatórios, porém desejados.

- Testes unitários
- Linter Ok!
- Code Formater Ok!(Eslint+ VScode config to force format, 
eu conscientemente decidi não usar o prettier.)

**Obs.: Lembrando que o uso de algum linter ou code formater irá depender da linguagem que sua API for criada**

# 🖥 O que desenvolver?

Você deverá criar uma API que o site [IMDb](https://www.imdb.com/) irá consultar para exibir seu conteúdo, sua API deve conter as seguintes features:

- Admin

  - Cadastro Ok!
  - Edição Ok!
  - Exclusão lógica (Desativação) Ok!

- Usuário

  - Cadastro Ok!
  - Edição Ok!
  - Exclusão lógica (Desativação) Ok!

- Filmes

  - Cadastro (Somente um usuário administrador poderá realizar esse cadastro) Ok!
  - Voto (A contagem dos votos será feita por usuário de 0-4 que indica quanto o usuário gostou do filme)Ok!
  - Listagem (deverá ter filtro por diretor, nome, gênero e/ou atores)Ok!
  - Detalhe do filme trazendo todas as informações sobre o filme, inclusive a média dos votos Ok!

**Obs.: Apenas os usuários poderão votar nos filmes e a API deverá validar quem é o usuário que está acessando, ou seja, se é admin ou não** Ok!

# 🔗 Links

- Documentação JWT https://jwt.io/
- Frameworks NodeJS:

  1. https://expressjs.com/pt-br/
  2. https://sailsjs.com/

- Guideline rails http://guides.rubyonrails.org/index.html
