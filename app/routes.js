const express = require('express');

const routes = express.Router();
const swaggerJsDoc = require('swagger-jsdoc');
const swaggerUi = require('swagger-ui-express');
const AdminController = require('./controllers/AdminController');
const UserController = require('./controllers/UserController');
const MovieController = require('./controllers/MovieController');
const AuthController = require('./controllers/AuthController');
const Auth = require('./middlewares/auth');

const swaggerOptions = require('../swaggerOptions');

const swaggerDocs = swaggerJsDoc(swaggerOptions);
routes.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocs));

/**
 * @swagger
 * /user:
 *  get:
 *    description: Use to get all common users
 *    tags: [User]
 *    responses:
 *      '200':
 *        description: A successful response
 *      '401':
 *        description: Unauthorized
 *      '500':
 *        description: Server Error
 *  post:
 *    description: Use to create a new common user
 *    tags: [User]
 *    parameters:
 *      - name: name
 *        in: body
 *        required: true
 *        schema:
 *          type : string
 *          example: "Jonas"
 *
 *      - name: password
 *        in: body
 *        required: true
 *        schema:
 *          type : string
 *          example: "123"
 *      - name: email
 *        in: body
 *        required: true
 *        schema:
 *          type : string
 *          example: "jonas@email.com"
 *
 *    responses:
 *      '200':
 *        description: A successful response
 *      '500':
 *        description: Server Error
 * /user/{id}:
 *  get:
 *    description: Use to get a common user by id
 *    tags: [User]
 *    parameters:
 *      - name: id
 *        in: path
 *        required: true
 *        description: user identifier
 *        schema:
 *          type : integer
 *          format: int64
 *          minimum: 1
 *    responses:
 *      '200':
 *        description: A successful response
 *      '401':
 *        description: Unauthorized
 *      '404':
 *        description: User not found
 *      '500':
 *        description: Server Error
 *  put:
 *    description: Use to edit a common user by id
 *    tags: [User]
 *    parameters:
 *      - name: id
 *        in: path
 *        required: true
 *        description: user identifier
 *        schema:
 *          type : integer
 *          format: int64
 *          minimum: 1
 *      - name: name
 *        in: body
 *        required: true
 *        schema:
 *          type : string
 *          example: "Jonas"
 *
 *      - name: password
 *        in: body
 *        required: true
 *        schema:
 *          type : string
 *          example: "123"
 *      - name: email
 *        in: body
 *        required: true
 *        schema:
 *          type : string
 *          example: "jonas@email.com"
 *
 *    responses:
 *      '200':
 *        description: A successful response
 *      '401':
 *        description: Unauthorized
 *      '404':
 *        description: User not found
 *      '500':
 *        description: Server Error
 *  delete:
 *    description: Use to soft delete a common user by id
 *    tags: [User]
 *    parameters:
 *      - name: id
 *        in: path
 *        required: true
 *        description: user identifier
 *        schema:
 *          type : integer
 *          format: int64
 *          minimum: 1
 *
 *
 *    responses:
 *      '204':
 *        description: A successful response
 *      '401':
 *        description: Unauthorized
 *      '500':
 *        description: Server Error
 * /vote/{idMovie}:
 *  post:
 *    description: Use to rate a movie by id
 *    tags: [User]
 *    parameters:
 *      - name: idMovie
 *        in: path
 *        required: true
 *        description: user identifier
 *        schema:
 *          type : integer
 *          format: int64
 *          minimum: 1
 *
 *
 *    responses:
 *      '200':
 *        description: A successful response
 *      '401':
 *        description: Unauthorized
 *      '404':
 *        description: Movie not found
 *      '500':
 *        description: Server Error
 * /admin:
 *  get:
 *    description: Use to get all admin users
 *    tags: [Admin]
 *    responses:
 *      '200':
 *        description: A successful response
 *      '401':
 *        description: Unauthorized
 *      '500':
 *        description: Server Error
 *  post:
 *    description: Use to create a new admin user
 *    tags: [Admin]
 *    parameters:
 *      - name: name
 *        in: body
 *        required: true
 *        schema:
 *          type : string
 *          example: "Jonas"
 *
 *      - name: password
 *        in: body
 *        required: true
 *        schema:
 *          type : string
 *          example: "123"
 *      - name: email
 *        in: body
 *        required: true
 *        schema:
 *          type : string
 *          example: "jonas@email.com"
 *
 *    responses:
 *      '200':
 *        description: A successful response
 *      '401':
 *        description: Unauthorized
 *      '500':
 *        description: Server Error
 * /admin/{id}:
 *  get:
 *    description: Use to get a admin user by id
 *    tags: [Admin]
 *    parameters:
 *      - name: id
 *        in: path
 *        required: true
 *        description: user identifier
 *        schema:
 *          type : integer
 *          format: int64
 *          minimum: 1
 *    responses:
 *      '200':
 *        description: A successful response
 *      '401':
 *        description: Unauthorized
 *      '404':
 *        description: User not found
 *      '500':
 *        description: Server Error
 *  put:
 *    description: Use to edit a admin user by id
 *    tags: [Admin]
 *    parameters:
 *      - name: id
 *        in: path
 *        required: true
 *        description: user identifier
 *        schema:
 *          type : integer
 *          format: int64
 *          minimum: 1
 *      - name: name
 *        in: body
 *        required: true
 *        schema:
 *          type : string
 *          example: "Jonas"
 *
 *      - name: password
 *        in: body
 *        required: true
 *        schema:
 *          type : string
 *          example: "123"
 *      - name: email
 *        in: body
 *        required: true
 *        schema:
 *          type : string
 *          example: "jonas@email.com"
 *
 *    responses:
 *      '200':
 *        description: A successful response
 *      '401':
 *        description: Unauthorized
 *      '404':
 *        description: User not found
 *      '500':
 *        description: Server Error
 *  delete:
 *    description: Use to soft delete a admin user by id
 *    tags: [Admin]
 *    parameters:
 *      - name: id
 *        in: path
 *        required: true
 *        description: user identifier
 *        schema:
 *          type : integer
 *          format: int64
 *          minimum: 1
 *
 *
 *    responses:
 *      '204':
 *        description: A successful response
 *      '401':
 *        description: Unauthorized
 *      '500':
 *        description: Server Error
 * /movie:
 *  get:
 *    description: Use to get all movies
 *    tags: [Movie]
 *    parameters:
 *      - name: name
 *        in: query
 *        required: false
 *        description: name filter string
 *      - name: writer
 *        in: query
 *        required: false
 *        description: writer filter string
 *      - name: director
 *        in: query
 *        required: false
 *        description: director filter string
 *      - name: category
 *        in: query
 *        required: false
 *        description: category filter string
 *
 *
 *
 *    responses:
 *      '200':
 *        description: A successful response
 *      '401':
 *        description: Unauthorized
 *      '500':
 *        description: Server Error
 *  post:
 *    description: Use to create a new movie
 *    tags: [Movie]
 *    parameters:
 *      - name: name
 *        in: body
 *        required: true
 *        schema:
 *          type : string
 *          example: "Azumi"
 *
 *      - name: director
 *        in: body
 *        required: false
 *        schema:
 *          type : string
 *          example: "Spielberg"
 *      - name: writer
 *        in: body
 *        required: false
 *        schema:
 *          type : string
 *          example: "Spielberg"
 *      - name: category
 *        in: body
 *        required: false
 *        schema:
 *          type : string
 *          example: "Animation"
 *    responses:
 *      '200':
 *        description: A successful response
 *      '401':
 *        description: Unauthorized
 *      '400':
 *        description: Name is required
 *      '500':
 *        description: Server Error
 * /movie/{id}:
 *  get:
 *    description: Use to get a movie by id
 *    tags: [Movie]
 *    parameters:
 *      - name: id
 *        in: path
 *        required: true
 *        description: movie identifier
 *        schema:
 *          type : integer
 *          format: int64
 *          minimum: 1
 *    responses:
 *      '200':
 *        description: A successful response
 *      '401':
 *        description: Unauthorized
 *      '404':
 *        description: movie not found
 *      '500':
 *        description: Server Error
 *  put:
 *    description: Use to edit a amovie by id
 *    tags: [Movie]
 *    parameters:
 *      - name: id
 *        in: path
 *        required: true
 *        description: movie identifier
 *        schema:
 *          type : integer
 *          format: int64
 *          minimum: 1
 *      - name: name
 *        in: body
 *        required: true
 *        schema:
 *          type : string
 *          example: "Azumi"
 *
 *      - name: director
 *        in: body
 *        required: false
 *        schema:
 *          type : string
 *          example: "Spielberg"
 *      - name: writer
 *        in: body
 *        required: false
 *        schema:
 *          type : string
 *          example: "Spielberg"
 *      - name: category
 *        in: body
 *        required: false
 *        schema:
 *          type : string
 *          example: "Animation"
 *
 *    responses:
 *      '200':
 *        description: A successful response
 *      '401':
 *        description: Unauthorized
 *      '404':
 *        description: Movie not found
 *      '500':
 *        description: Server Error
 *  delete:
 *    description: Use to soft delete a movie by id
 *    tags: [Movie]
 *    parameters:
 *      - name: id
 *        in: path
 *        required: true
 *        description: movie identifier
 *        schema:
 *          type : integer
 *          format: int64
 *          minimum: 1
 *
 *
 *    responses:
 *      '204':
 *        description: A successful response
 *      '401':
 *        description: Unauthorized
 *      '500':
 *        description: Server Error
 * /login:
 *  post:
 *    description: Use to authenticate
 *    tags: [Auth]
 *    parameters:
 *      - name: email
 *        in: body
 *        required: true
 *        schema:
 *          type : string
 *          example: "usuario123"
 *      - name: password
 *        in: body
 *        required: true
 *        schema:
 *          type : string
 *          example: "123"
 *    responses:
 *      '200':
 *        description: A successful response resturns user token
 *      '404':
 *        description: User not found
 *      '500':
 *        description: Server Error
 */

// User endpoints
routes.get('/user', Auth.JWTAuth(['User', 'Admin']), UserController.index);
routes.get('/user/:id', Auth.JWTAuth(['User', 'Admin']), UserController.show);
routes.post('/user', UserController.store);
routes.put('/user/:id', Auth.JWTAuth(['User', 'Admin']), UserController.update);
routes.delete('/user/:id', Auth.JWTAuth(['User', 'Admin']), UserController.delete);
routes.post('/vote/:idMovie', Auth.JWTAuth(['User']), UserController.vote);
// Admin endpoints
routes.get('/admin', Auth.JWTAuth(['Admin']), AdminController.index);
routes.get('/admin/:id', Auth.JWTAuth(['Admin']), AdminController.show);
routes.post('/admin', Auth.JWTAuth(['Admin']), AdminController.store);
routes.put('/admin/:id', Auth.JWTAuth(['Admin']), AdminController.update);
routes.delete('/admin/:id', Auth.JWTAuth(['Admin']), AdminController.delete);

// Movie endpoints
routes.get('/movie', Auth.JWTAuth(['User', 'Admin']), MovieController.index);
routes.get('/movie/:id', Auth.JWTAuth(['User', 'Admin']), MovieController.show);
routes.post('/movie', Auth.JWTAuth(['Admin']), MovieController.store);
routes.put('/movie/:id', Auth.JWTAuth(['Admin']), MovieController.update);
routes.delete('/movie/:id', Auth.JWTAuth(['Admin']), MovieController.delete);
// Auth endpoints
routes.post('/login', AuthController.store);

module.exports = routes;
