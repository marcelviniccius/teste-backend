const bcrypt = require('bcryptjs');

module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    name: DataTypes.STRING,
    email: DataTypes.STRING,
    password: DataTypes.STRING,
    isDeleted: DataTypes.BOOLEAN,
    role: DataTypes.STRING,

  },
  { timestamps: true });
  User.beforeCreate(async (model) => {
    if (model.password) {
      model.password = await bcrypt.hash(model.password, 8);
    }
  });
  sequelize.models.Rate.belongsTo(sequelize.models.User, { foreignKey: 'id' });
  sequelize.models.Rate.belongsTo(sequelize.models.Movie, { foreignKey: 'id' });
  sequelize.models.User.hasMany(sequelize.models.Rate, { foreignKey: 'idUser' });
  sequelize.models.Movie.hasMany(sequelize.models.Rate, { foreignKey: 'idMovie' });

  return User;
};
