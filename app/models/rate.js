module.exports = (sequelize, DataTypes) => {
  const Rate = sequelize.define('Rate', {
    idMovie: DataTypes.INTEGER,
    idUser: DataTypes.INTEGER,
    rate: DataTypes.INTEGER,
    isDeleted: DataTypes.BOOLEAN,

  },
  { timestamps: true });

  return Rate;
};
