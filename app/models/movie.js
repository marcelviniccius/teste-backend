module.exports = (sequelize, DataTypes) => {
  const Movie = sequelize.define('Movie', {
    name: DataTypes.STRING,
    director: DataTypes.STRING,
    writer: DataTypes.STRING,
    category: DataTypes.STRING,
    isDeleted: DataTypes.BOOLEAN,

  },
  { timestamps: true });

  return Movie;
};
