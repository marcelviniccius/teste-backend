const jwt = require('jsonwebtoken');
require('dotenv').config();
const models = require('../models');

const { User } = models;
exports.JWTAuth = function async(roles) {
  // eslint-disable-next-line consistent-return
  return async (req, res, next) => {
    const { authorization } = req.headers;
    if (!authorization) return res.status(401).send('Token not provided!');
    // eslint-disable-next-line no-unused-vars
    const [bearer, token] = authorization.split(' ');
    try {
      const user = jwt.verify(token, process.env.JWT_KEY);
      const userData = await User.findOne({ where: { email: user.email } });

      if (!roles.find((role) => role === userData.dataValues.role)) {
        return res.status(401).json({ message: 'You Do Not Have Permission!' });
      }
      req.authenticatedUser = userData.dataValues;

      next();
    } catch (error) {
      res.status(401).json({ message: 'Invalid Token!', error: error.message });
    }
  };
};
