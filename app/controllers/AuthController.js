const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const models = require('../models');
require('dotenv').config();

const { User } = models;
class AuthController {
  async store(req, res) {
    const { email, password } = req.body;
    let user;
    try {
      user = await User.findOne({ where: { email, isDeleted: false } });
      if (!user) res.status(404).json({ message: 'User not found!' });
      const correctPassword = await bcrypt.compare(password, user.password);
      if (!correctPassword) res.status(401).json({ message: 'Wrong Password or Email!' });
      res.status(200).json({ token: jwt.sign({ email: user.email, name: user.name, id: user.id }, process.env.JWT_KEY, { expiresIn: '15d' }) });
    } catch (error) {
      res.status(500).json({ message: 'Server Error!', error: error.message });
    }
  }
}
module.exports = new AuthController();
