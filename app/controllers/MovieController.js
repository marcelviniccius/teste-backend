const models = require('../models');

const { Movie } = models;
class MovieController {
  async index(req, res) {
    const where = { isDeleted: false };
    const {
      name, director, category, writer,
    } = req.query;
    if (name) {
      where.name = name;
    }
    if (director) {
      where.director = director;
    }
    if (writer) {
      where.writer = writer;
    }
    if (category) {
      where.category = category;
    }
    let movies;
    try {
      movies = await Movie.findAll({
        attributes: ['id', 'name', 'director', 'writer', 'category', 'createdAt', 'updatedAt'],
        include: models.Rate,
        where,
      });
    } catch (error) {
      res.status(500).json({ message: 'Server error!', error: error.message });
    }
    res.status(200).json(movies);
  }

  async show(req, res) {
    let movie;
    let totalrate;
    try {
      movie = await Movie.findByPk(req.params.id, { attributes: ['id', 'name', 'director', 'writer', 'isDeleted', 'createdAt', 'updatedAt'], include: models.Rate });
      const { isDeleted } = movie;

      if (!movie || isDeleted) return res.status(404).json({ message: 'Movie not found!' });
      totalrate = 0;
      const movieData = movie.toJSON();
      const rates = movieData.Rates;

      // eslint-disable-next-line no-restricted-syntax
      for (const rate of rates) {
        totalrate += rate.rate;
      }

      totalrate /= rates.length;
      if (!movie) res.status(404).json({ message: 'User not found!' });
    } catch (error) {
      return res.status(500).json({ message: 'Server Error!', error: error.message });
    }

    return res.status(200).json({ movie, totalrate: totalrate || 0 });
  }

  async store(req, res) {
    const {
      name, director, writer, category,
    } = req.body;
    let movie;
    if (!name) {
      res.status(400).json({ message: 'Field name is required!' });
    }

    try {
      movie = await Movie.create({
        name, director, writer, category,
      });
    } catch (error) {
      res.status(500).json({ message: 'Server Error!', error: error.message });
    }

    res.status(200).json(movie);
  }

  async update(req, res) {
    const {
      name, director, writer, category,
    } = req.body;
    try {
      const movieAlreadyExists = await Movie
        .findByPk(req.params.id, { where: { isDeleted: false } });
      if (!movieAlreadyExists) {
        res.status(404).json({ message: 'User Not Found!' });
        return;
      }
      await Movie.update({
        name, director, writer, category,
      }, { where: { id: req.params.id, isDeleted: false } });
    } catch (error) {
      res.status(500).json({ message: 'Server Error!', error });
      return;
    }
    res.status(200).json({
      name, director, writer, category,
    });
  }

  async delete(req, res) {
    try {
      await Movie.update({ isDeleted: true }, { where: { id: req.params.id } });
    } catch (error) {
      res.status(500).json({ message: 'Server Error!', error: error.message });
    }
    res.status(204).json({});
  }
}
module.exports = new MovieController();
