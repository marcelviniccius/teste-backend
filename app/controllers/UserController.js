const models = require('../models');

const { User, Rate, Movie } = models;
class UserController {
  async index(req, res) {
    let users;
    try {
      users = await User.findAll({ attributes: ['id', 'name', 'email', 'createdAt', 'updatedAt'], where: { isDeleted: false, role: 'User' } });
    } catch (error) {
      res.status(500).json({ message: 'Server error!', error: error.message });
    }
    res.status(200).json(users);
  }

  async show(req, res) {
    let user;
    try {
      user = await User.findByPk(req.params.id, { attributes: ['id', 'name', 'email', 'isDeleted', 'role', 'createdAt', 'updatedAt'] });
      const { isDeleted, role } = user;

      if (!user || isDeleted || role !== 'User') return res.status(404).json({ message: 'User not found!' });
    } catch (error) {
      return res.status(500).json({ message: 'Server Error!', error: error.message });
    }

    const {
      id, name, email, createdAt, updatedAt,
    } = user;
    return res.status(200).json({
      id, name, email, createdAt, updatedAt,
    });
  }

  async store(req, res) {
    const { name, email, password } = req.body;
    let user;
    let userAlreadyExists;
    try {
      userAlreadyExists = await User.findOne({ where: { email } });
      if (userAlreadyExists) res.status(400).json({ message: 'User Already exists!' });
    } catch (error) {
      res.status(500).json({ message: 'Server Error!', error: error.message });
    }
    if (!name || !email || !password) {
      res.status(400).json({ message: 'Fields name, email and password are required!' });
    }

    try {
      user = await User.create({ name, email, password });
    } catch (error) {
      res.status(500).json({ message: 'Server Error!', error: error.message });
    }

    res.status(200).json(user);
  }

  async update(req, res) {
    const { name, email } = req.body;
    try {
      const userAlreadyExists = await User.findByPk(req.params.id, { attributes: ['id', 'name', 'email', 'isDeleted', 'role', 'createdAt', 'updatedAt'] });
      const { isDeleted, role } = userAlreadyExists;

      if (!userAlreadyExists || isDeleted || role !== 'User') return res.status(404).json({ message: 'User not found!' });

      await User.update({ name, email }, { where: { id: req.params.id, isDeleted: false, role: 'User' } });
    } catch (error) {
      return res.status(500).json({ message: 'Server Error!', error });
    }
    return res.status(200).json({ name, email });
  }

  async delete(req, res) {
    try {
      await User.update({ isDeleted: true }, { where: { id: req.params.id, role: 'User' } });
    } catch (error) {
      res.status(500).json({ message: 'Server Error!', error: error.message });
    }
    res.status(204).json({});
  }

  async vote(req, res) {
    let { rate } = req.body;
    rate = Math.floor(rate);
    if (rate > 4 || rate < 0) res.status(400).json({ message: 'Rate must be between 0 and 4!' });
    let upsertedRate;
    const loggedID = req.authenticatedUser.id;
    try {
      const movieExists = await Movie.findOne(
        { where: { id: req.params.idMovie } },
      );
      if (!movieExists) return res.status(400).json({ message: "Movie doesn't exists!" });
      const rateAlreadyExists = await Rate.findOne(
        { where: { idUser: loggedID, idMovie: req.params.idMovie } },
      );
      if (rateAlreadyExists) {
        await Rate.update({ rate },
          {
            where: {
              id: rateAlreadyExists.dataValues.id,

            },
          });
      } else {
        upsertedRate = await Rate.create(
          { rate, idUser: loggedID, idMovie: req.params.idMovie },
        );
      }
    } catch (error) {
      return res.status(500).json({ message: 'Server Error!', error });
    }
    return res.status(200).json(upsertedRate);
  }
}
module.exports = new UserController();
