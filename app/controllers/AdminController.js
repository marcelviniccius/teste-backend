const models = require('../models');

const { User } = models;
class AdminController {
  async index(req, res) {
    let users;
    try {
      users = await User.findAll({ attributes: ['id', 'name', 'email', 'createdAt', 'updatedAt'], where: { isDeleted: false, role: 'Admin' } });
    } catch (error) {
      res.status(500).json({ message: 'Server error!', error: error.message });
    }
    res.status(200).json(users);
  }

  async show(req, res) {
    let user;
    try {
      user = await User.findByPk(req.params.id, { attributes: ['id', 'name', 'email', 'isDeleted', 'role', 'createdAt', 'updatedAt'] });
      const { isDeleted, role } = user;

      if (!user || isDeleted || role !== 'Admin') return res.status(404).json({ message: 'Admin not found!' });
    } catch (error) {
      return res.status(500).json({ message: 'Server Error!', error: error.message });
    }
    const {
      id, name, email, createdAt, updatedAt,
    } = user;
    return res.status(200).json({
      id, name, email, createdAt, updatedAt,
    });
  }

  async store(req, res) {
    const { name, email, password } = req.body;
    let user;
    let userAlreadyExists;
    try {
      userAlreadyExists = await User.findOne({ where: { email } });
      if (userAlreadyExists) res.status(400).json({ message: 'User Already exists!' });
    } catch (error) {
      res.status(500).json({ message: 'Server Error!', error: error.message });
    }
    if (!name || !email || !password) {
      res.status(400).json({ message: 'Fields name, email and password are required!' });
    }

    try {
      const role = 'Admin';
      user = await User.create({
        name, email, password, role,
      });
    } catch (error) {
      res.status(500).json({ message: 'Server Error!', error: error.message });
    }
    res.status(200).json(user);
  }

  async update(req, res) {
    const { name, email } = req.body;
    try {
      const adminAlreadyExists = await User.findByPk(req.params.id, { attributes: ['id', 'name', 'email', 'isDeleted', 'role', 'createdAt', 'updatedAt'] });
      const { isDeleted, role } = adminAlreadyExists;

      if (!adminAlreadyExists || isDeleted || role !== 'Admin') return res.status(404).json({ message: 'Admin not found!' });

      await User.update({ name, email }, { where: { id: req.params.id, isDeleted: false } });
    } catch (error) {
      return res.status(500).json({ message: 'Server Error!', error: error.message });
    }
    return res.status(200).json({ name, email });
  }

  async delete(req, res) {
    try {
      await User.update({ isDeleted: true }, { where: { id: req.params.id, role: 'Admin' } });
    } catch (error) {
      res.status(500).json({ message: 'Server Error!', error: error.message });
    }
    res.status(204).json({});
  }
}
module.exports = new AdminController();
